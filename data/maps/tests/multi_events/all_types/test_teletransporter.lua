--[[
	This unit test for the multi_events script verifies that the
	teletransporter:on_activated() event is triggered
]]

local map = ...
local game = map:get_game()
local hero = game:get_hero()

require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

function map:on_opening_transition_finished()
	local _,_,layer = hero:get_position()
	local teletransporter = map:create_teletransporter{ --creates teletransporter at hero's location
		x = 0, y = 0, layer = layer,
		width = 32, height = 32,
		destination_map = map:get_id(),
		enabled_at_start = false,
	}
	teletransporter:register_event("on_activated", function(self)
		self:set_enabled(false)
		events_proto:log(sol.main.get_type(self))
	end)
	
	local end_test_cb --(function) call after hero teleportation is complete
	events_proto:set_trigger(function(callback)
		end_test_cb=callback
		teletransporter:set_enabled(true)
		return true --true means this function will call the callback manually after a delay
	end)
	events_proto:trigger"teletransporter"
	
	--have to wait one cycle for teletransporter event to be triggered
	sol.timer.start(self, 10, function()
		end_test_cb()
		events_proto:exit() --verifies all tests have finished running before exit
	end)
end
