--[[
	This unit test for the multi_events script verifies the destination:on_activated event
	gets triggered.
]]

local map = ...
local game = map:get_game()
local hero = game:get_hero()

require"scripts/multi_events"
local events_proto = require"tests/events_prototype"



local end_test_cb --(function) call after hero teleportation is complete
destination:register_event("on_activated", function(self)
	events_proto:log(self:get_name())
end)

function map:on_opening_transition_finished()
	local is_done = game:get_value"is_done" --only teleport hero first time entering map
	
	events_proto:set_trigger(function(callback)
		hero:teleport(map:get_id(), "destination", "immediate")
		end_test_cb=callback
		return true
	end)
	if not is_done then
		game:set_value("is_done", true)
		events_proto:trigger"destination" --teleports hero to destination
	end
	
	sol.timer.start(self, 20, function() --wait 20 ms for teleportation to complete
		end_test_cb() --check if test passed
		events_proto:exit() --verifies all tests have finished running before exit
	end)
end
